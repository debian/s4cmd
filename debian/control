Source: s4cmd
Maintainer: Sascha Steinbiss <satta@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               bash-completion,
               help2man,
               python3-all,
               python3-boto3 (>= 1.3.1),
               python3-setuptools,
               python3-urllib3,
               python3-tz
Standards-Version: 4.6.1
Homepage: https://github.com/bloomreach/s4cmd
Vcs-Git: https://salsa.debian.org/debian/s4cmd.git
Vcs-Browser: https://salsa.debian.org/debian/s4cmd

Package: s4cmd
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-tz
Description: Super Amazon S3 command line tool
 The s4cmd tool provides a command line interface for interaction with
 Amazon's S3 cloud storage service. It is intended as an alternative to
 the existing s3cmd tool, enhancing performance and support for large
 files, and coming with a number of additional features and fixes.
 .
 s4cmd strives to be compatible with the most common usage scenarios for
 s3cmd. It does not offer exact drop-in compatibility, due to a number of
 corner cases where different behavior seems preferable, or for bugfixes.
