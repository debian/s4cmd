[DESCRIPTION]
S4cmd is a command-line utility for accessing Amazon S3, inspired by s3cmd.
It is intended as an alternative to s3cmd for enhanced performance and for
large files, and with a number of additional features and fixes.
In general, it strives to be compatible with the most common usage scenarios
for s3cmd. It does not offer exact drop-in compatibility, due to a number of
corner cases where different behavior seems preferable, or for bugfixes.

[COPYRIGHT]
2012-2016 BloomReach, Inc.

[REPORTING BUGS]
Please report bugs with s4cmd itself to the authors' GitHub issue tracker:
https://github.com/bloomreach/s4cmd/issues

